## Flowerpunk
This site was built using [React Router Redux Boilerplate](https://github.com/northbank/react-router-redux-boilerplate).

Run dev environment
===================

1. `npm install`
2. `npm start`

Run production build
====================

1. `npm install`
2. `npm run build`