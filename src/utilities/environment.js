export function isNode() {
  return process && process.title && process.title === 'node';
}

export function hasWindow() {
  return typeof window !== 'undefined' && window !== null;
}

export function getApiUrl() {
  if (isNode() && process.env.NODE_ENV === 'production') {
    return 'http://34.198.157.92:3000/';
  } else {
    return 'http://localhost:3000/api/';
  }
}

export function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export function handleErrors(response) {
  if (response.success === false) {
    throw Error(response.error);
  }

  return response;
}

export function getNavigatorInfo() {
  if (hasWindow() && window.hasOwnProperty('navigator')) {
    return {
      platform: window.navigator.platform,
      appCodeName: window.navigator.appCodeName,
      appName: window.navigator.appName,
      appVersion: window.navigator.appVersion,
      language: window.navigator.language,
      oscpu: window.navigator.oscpu,
      userAgent: window.navigator.userAgent,
      vendor: window.navigator.vendor
    }
  }

  return {};
}