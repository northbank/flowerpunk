import React from 'react';
import {NavLink} from 'react-router-dom';

const Thumbnail = ({route}) => (
  <div className="col-xs-6 col-sm-3 col-md-2">
    <div className="thumbnail">
      <div
        className="thumbnail-image"
        style={{
        backgroundImage: 'url(' + route.img + ')',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
      }}/>
      <div className="caption">
        <NavLink
          to={route.path}><h3>{route.title}</h3></NavLink>
        <p>{route.description}</p>
      </div>
    </div>
  </div>
);

export default Thumbnail;