import React from 'react';
import {updatePayload, contact} from '../../actions/contact';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.verifiedSubmit = this.verifiedSubmit.bind(this);
    this.getMessageBox = this.getMessageBox.bind(this);

    this.state = {
      submitted: false,
      email: '',
      name: '',
      message: '',
      invalid: []
    }
  }

  static validatePhone(phone) {
    if (!phone || !phone.match(/\d/g)) {
      return false;
    }

    return phone.match(/\d/g).length === 10;
  }

  static validateEmail(email) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
  }

  verifiedSubmit() {
    const {email, name, message} = this.props.payload;

    this.props.contact({
      email: email,
      name: name,
      message: message
    });
  }

  submit() {
    const {email, name, message} = this.props.payload;
    const invalid = [];

    if (!Contact.validateEmail(email) && !Contact.validatePhone(email)) {
      invalid.push('email');
    }

    if (name.length < 2) {
      invalid.push('name');
    }

    if (message.length < 2) {
      invalid.push('message');
    }

    if (invalid.length === 0) {
      this.setState({
        submitted: true,
        invalid: invalid
      });

      this.verifiedSubmit()
    } else {
      this.setState({
        invalid: invalid
      });
    }
  }

  getMessageBox() {
    const {error, payload, loading, success} = this.props;
    const {email, name, message} = payload;
    const {invalid} = this.state;

    if (success) {
      return <div>
        <h2>Thank you.</h2>
        <p>Your message has been received. Expect a response within a day. </p>
      </div>
    }

    if (error) {
      return <div>
        <h2>Oh no!</h2>
        <p>Something went wrong.</p>
        <p>{error.message || 'Sorry!'}</p>
        <p>Please email me at stantonr@bu.edu</p>
        <p>(if you're still interested, that is)</p>
      </div>
    }

    let emailClass = invalid.indexOf('email') > -1 ? 'has-error' : '';
    let nameClass = invalid.indexOf('name') > -1 ? 'has-error' : '';
    let messageClass = invalid.indexOf('message') > -1 ? 'has-error' : '';

    let button = (
      <button
      disabled={success || loading || error}
      id="submit"
      onClick={this.submit}
      className="button">{loading ? <i className="fa fa-cog fa-spin" /> : 'Send Message'}
      </button>
    );

    return (
      <div>
        <form>
          <div className={"form-group " + emailClass}>
            <input
              onChange={(e) => this.props.updatePayload('email', e.target.value)}
              value={email}
              type="email"
              className="form-control form-transparent"
              id="email"
              placeholder="Email or phone"/>
          </div>
          <div className={"form-group " + nameClass}>
            <input
              onChange={(e) => this.props.updatePayload('name', e.target.value)}
              value={name}
              type="name"
              className="form-control form-transparent"
              id="name"
              placeholder="Name"/>
          </div>
          <div className={"form-group " + messageClass}>
              <textarea
                className="form-control form-transparent"
                id="message"
                placeholder="Please tell me about your project and how I can help."
                rows="3"
                onChange={(e) => this.props.updatePayload('message', e.target.value)}
                value={message}
              />
          </div>
        </form>
        {button}
      </div>
    );
  }

  render() {
    let messageBox = this.getMessageBox();

    return (
      <div className="contact-container text-center">
        <h4>Let's build something.</h4>
        {messageBox}
      </div>
    );
  }
}


const mapStateToProps = state => {
  const {contact} = state;
  const {
    open,
    selected,
    payload,
    loading,
    error,
    success
  } = contact || {
    open: false,
    selected: 'menu',
    payload: {
      email: '',
      name: '',
      message: ''
    },
    loading: false,
    error: null,
    success: false
  };

  return {open, selected, payload, loading, error, success};
};

const mapDispatchToProps = dispatch => {
  return {
    updatePayload: bindActionCreators(updatePayload, dispatch),
    contact: bindActionCreators(contact, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Contact);