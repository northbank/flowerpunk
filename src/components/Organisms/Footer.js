import React from 'react';
import Contact from './../Molecules/Contact';

class Footer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="footer">
        <div className="col-xs-12 col-sm-8 col-sm-offset-2">
          <Contact />
        </div>
        <div className="col-xs-12">
          <div className="grid">
            <div className="justify">
              <a target="_blank" href="http://linkedin.com/in/samstantonreid" className="round-icon justify">
                <i className="fa fa-2x fa-linkedin" aria-hidden="true"/>
              </a>
              <a target="_blank" href="http://facebook.com/samstantonreid" className="round-icon justify">
                <i className="fa fa-2x fa-facebook" aria-hidden="true"/>
              </a>
              <a target="_blank" href="http://github.com/northbank" className="round-icon justify">
                <i className="fa fa-2x fa-github" aria-hidden="true"/>
              </a>
            </div>
          </div>
          <p className="text-center">Sam Stanton-Reid 2017</p>
        </div>
      </div>
    );
  }
}

export default Footer;
