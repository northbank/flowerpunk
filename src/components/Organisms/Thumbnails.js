import React from 'react';
import Thumbnail from './../Molecules/Thumbnail';

const Thumbnails = ({routes}) => (
  <div className="panel panel-default">
    <div className="panel-body">
      <p>Select an example</p>
      {routes.map((route, i) => (
        <Thumbnail
          route={route}
          key={i}/>
      ))}
    </div>
  </div>
);

export default Thumbnails;