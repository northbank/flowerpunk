import React from "react";
import * as d3 from "d3";
import Versor from "../../utilities/versor";

const cos = Math.cos,
  PI = Math.PI,
  sin = Math.sin,
  sqrt = Math.sqrt,
  atan2 = Math.atan2,
  exp = Math.exp,
  sq = (a) => a * a,
  acos = Math.acos,
  abs = Math.abs;

class Sphere extends React.Component {
  constructor(props) {
    super(props);

    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.renderSphereSpirals = this.renderSphereSpirals.bind(this);
    this.rotate = this.rotate.bind(this);
    this.createPolygons = this.createPolygons.bind(this);
    this.mouseDown = false;

    this.v0 = 0;
    this.r0 = 0;
    this.q0 = 0;

    this.loxodromes = this.createPolygons(this.loxodrome());

    this.loxodromesInverse = JSON.parse(JSON.stringify(this.loxodromes))
      .map(feature => {
        feature.coordinates[0].map(point => {
          point[1] *= -1;
          return point;
        });

        return feature;
      });

    this.state = {
      rotation: [0, -25, 0]
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.radius !== this.props.radius) {
      this.setState({
        rotation: [0, -25, 0]
      })
    }

    if (newProps.spin !== this.props.spin) {
      if (newProps.spin) {
        this.interval = setInterval(() => this.rotate(), 40);
      } else {
        clearInterval(this.interval);
      }
    }
  }

  componentDidMount() {
    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('mouseup', this.onMouseUp);

    if (this.props.spin) {
      this.interval = setInterval(() => this.rotate(), 40);

    }
  }

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('mouseup', this.onMouseUp);

    if (this.props.spin) {
      clearInterval(this.interval);
    }
  }

  // Converts [theta, phi] to [lng, lat]
  sphericalToGeo(p) {
    let lng = p[0] * 180 / PI;
    let lat = p[1] * 180 / PI - 90;

    return [lng, lat];
  }

  // Converts [x, y, z] to [theta, phi]
  cartesianToSpherical(p) {
    let radius = sqrt(sq(p[0]) + sq(p[1]) + sq(p[2]));

    let theta = atan2(p[0], p[1]),
      phi = acos(p[2] / radius);

    return [theta, phi];
  }

  loxodrome(zScale = 1, m = 1.8) {
    const T = [-4 * PI, PI * 4];
    const range = T[1] - T[0];

    let loxodromeMatrix = [...Array(8)].map(() => []);

    for (let t = T[0]; t <= T[1]; t += range / 250) {
      let r = exp(-1 * t / m);

      let x = 2 * r * cos(t),
        y = 2 * r * sin(t),
        z = zScale * (1 - sq(r));

      //convert to spherical, offset the angle and duplicate to create matrix of 8 evenly spaced loxodromes
      for (let i = 0; i < 8; i++) {
        let spherical = this.cartesianToSpherical([x, y, z]);

        spherical[0] += PI / 4 * i;
        loxodromeMatrix[i].push(this.sphericalToGeo(spherical));
      }
    }

    return loxodromeMatrix;
  }

  createPolygons(loxodromeMatrix) {
    let polygons = [];

    for (let i = 0; i < 8; i += 2) {
      let arr = loxodromeMatrix[i];
      let reversed = this.reverse(loxodromeMatrix[i + 1]);

      arr = arr.concat(reversed);
      arr.push(arr[0]);

      let feature = {type: 'Polygon', coordinates: [arr]};

      polygons.push(feature);
    }

    return polygons;
  }

  onTouchStart(e) {
    e.preventDefault();


    this.onMouseDown(e.touches[0])
  }

  onTouchMove(e) {
    e.preventDefault();


    this.onMouseMove(e.touches[0])
  }

  onMouseDown(e) {
    this.mouseDown = true;
    const {radius, spin} = this.props;

    if (spin) {
      clearInterval(this.interval);
    }

    const projection = d3.geoOrthographic()
      .rotate(this.state.rotation)
      .scale(radius)
      .translate([radius, radius]);

    const offset = this.sphere.getBoundingClientRect();

    this.v0 = Versor.cartesian(projection.invert([e.clientX - offset.left, e.clientY - offset.top]));
    this.r0 = projection.rotate();
    this.q0 = Versor.versor(this.r0);
  }

  onMouseMove(e) {
    if (this.mouseDown) {
      const radius = this.props.radius;

      const projection = d3.geoOrthographic()
        .rotate(this.state.rotation)
        .scale(radius)
        .translate([radius, radius]);

      const offset = this.sphere.getBoundingClientRect();
      const xOffset = e.clientX - offset.left;
      const yOffset = e.clientY - offset.top;

      const v1 = Versor.cartesian(projection.rotate(this.r0).invert([xOffset, yOffset]));
      const q1 = Versor.multiply(this.q0, Versor.delta(this.v0, v1));

      this.setState({
        rotation: Versor.rotation(q1)
      });
    }
  }

  onMouseUp() {
    if (this.mouseDown) {
      this.mouseDown = false;

      if (this.props.spin) {
        this.interval = setInterval(() => this.rotate(), 30);
      }
    }
  }

  rotate() {
    if (this.mouseDown) {
      return;
    }

    let rotation = this.state.rotation;
    rotation[0] += 0.2;
    rotation[1] -= 0.1;

    this.setState({
      rotation: rotation
    });
  }

  reverse(arr) {
    for (let i = 0; i < arr.length / 2; i++) {
      let temp = arr[i];
      arr[i] = arr[arr.length - i - 1];
      arr[arr.length - i - 1] = temp;
    }
    return arr;
  }

  renderSphereSpirals(path, fill, loxodromes) {
    if (!loxodromes) {
      return null;
    }

    return loxodromes.map((feature, i) => {
      let arr = {...feature};

      // exterior ring for polygons smaller than a hemisphere must be clockwise
      // those larger than a hemisphere must be anticlockwise
      if (path.area(arr) > sq(this.props.radius) * 2) {
        this.reverse(arr.coordinates[0]);
      }

      return <path
        key={i}
        d={path(arr)}
        strokeWidth="0"
        fill={fill}
      />
    });
  }

  renderFeatures() {
    const radius = this.props.radius;

    const projection = d3.geoOrthographic()
      .rotate(this.state.rotation)
      .scale(radius)
      .translate([radius, radius]);
    const projectionInverse = d3.geoOrthographic()
      .rotate(this.state.rotation)
      .scale(-1 * radius, radius)
      .translate([radius, radius]);

    const path = d3.geoPath(projection);
    const pathInverse = d3.geoPath(projectionInverse);

    const sphereInside = this.renderSphereSpirals(pathInverse, 'rgba(90, 50, 50, 1)', this.loxodromesInverse);
    const sphereOutside = this.renderSphereSpirals(path, 'rgba(255, 0, 0, .85)', this.loxodromes);

    return (
      <g>
        {sphereInside}
        {sphereOutside}
      </g>
    );
  }

  render() {
    const radius = abs(this.props.radius);

    return (
      <div
        id="loxo-sphere"
        ref={sphere => this.sphere = sphere}
        onMouseDown={this.onMouseDown}
        onTouchStart={this.onTouchStart}
        onTouchEnd={this.onMouseUp}
        onTouchMove={this.onTouchMove}
      >
        <svg
          width={radius * 2}
          height={radius * 2}
        >
          {this.renderFeatures()}
        </svg>
      </div>
    );
  }
}

Sphere.defaultProps = {
  radius: 200,
  spin: true
};

export default Sphere;