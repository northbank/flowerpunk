import React from 'react';
import {NavLink} from 'react-router-dom';
import ContactOverlay from './ContactOverlay';
import OpenContact from './../Atoms/OpenContact';

class Nav extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const routes = this.props.routes;

    const navLinks = routes.map(route => {
      return (
        <li key={route.path}>
          <NavLink
            exact={route.exact}
            to={route.path}
            activeStyle={{color: 'rgba(255, 0, 0, 1)'}}>
            {route.title}
          </NavLink>
        </li>
      );
    });

    return (
      <nav className="navbar navbar-default">
        <ContactOverlay />
        <div className="container-fluid">
            <ul className="nav navbar-nav">
              {navLinks}
              <li>
                <OpenContact>
                  Contact <i className="fa fa-pencil-square-o" />
                </OpenContact>
              </li>
            </ul>
        </div>
      </nav>
    );
  }
}

Nav.defaultProps = {
  routes: []
};

export default Nav;