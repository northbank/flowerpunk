import React from 'react';
import {openContact, closeContact} from '../../actions/contact';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import Contact from './../Molecules/Contact';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import {withRouter} from 'react-router-dom'

const Spin = ({children, ...props}) => (
  <CSSTransition
    {...props}
    timeout={600}
    classNames="spin"
  >
    {children}
  </CSSTransition>
);

const PageBlock = props => (
  <CSSTransition
    {...props}
    timeout={600}
    classNames="fade"
  >
    <div className="justify contact-block colored"/>
  </CSSTransition>
);

class ContactOverlay extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);

    this.state = {
      animation: '',
      open: false
    }
  }

  toggle() {
    if (!this.props.open) {
      this.props.openContact('contact');
    } else {
      this.props.closeContact();
    }
  }

  componentWillReceiveProps(newProps) {
    if (this.props.location.pathname !== newProps.location.pathname) {
      this.props.closeContact();
    }
  }

  render() {
    const {open, closeContact} = this.props;

    const spin = (
      <Spin>
        <div
          onClick={closeContact}
          className="justify contact-block">
          <div
            onClick={e => e.stopPropagation()}
            className="contact-inner justify">
            <div
              onClick={closeContact}
              className="contact-icon justify">
              <i className="fa fa-close fa-4x"/>
            </div>
            <Contact />
          </div>
        </div>
      </Spin>
    );

    const block = open ? <PageBlock /> : null;
    const inner = open ? spin : null;

    return (
        <TransitionGroup>
          {block}
          {inner}
        </TransitionGroup>
    );
  }
}

const mapStateToProps = state => {
  const {contact} = state;
  const {
    open
  } = contact || {
    open: false
  };

  return {open};
};

const mapDispatchToProps = (dispatch) => {
  return {
    openContact: bindActionCreators(openContact, dispatch),
    closeContact: bindActionCreators(closeContact, dispatch)
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ContactOverlay));
