import React from 'react';
import {NavLink} from 'react-router-dom';
import {withRouter} from 'react-router-dom'
import RouteWithSubRoutes from './../Molecules/RouteWithSubRoutes';
import Thumbnails from './../Organisms/Thumbnails';

class Examples extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {match, routes} = this.props;

    return (
      <div className="container">
        <h3>
          <NavLink to={match.url}>Example Gallery</NavLink>
        </h3>
        {match.isExact ? <Thumbnails match={match} routes={routes}/> : null}
        {routes.map((route, i) => (
          <RouteWithSubRoutes
            component={route.component}
            path={route.path}
            key={i}/>
        ))}
      </div>
    )
  }
}

export default withRouter(Examples);