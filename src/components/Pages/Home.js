import React from 'react';
import Button from './../Atoms/Button';
import Escher from './Escher';
import OpenContact from './../Atoms/OpenContact';
import {withRouter} from 'react-router-dom'
import {bindActionCreators} from 'redux'

const icons = [
  "d3js",
  "Babel",
  "HTML5",
  "NGINX",
  "Git",
  "React",
  "IntelliJ",
  "PHP",
  "SASS",
  "Python",
  "Protractor",
  "Javascript",
  "Laravel",
  "Linux",
  "NodeJS",
  "Mysql",
  "Mongodb",
  "Vim",
  "Gulp"
];

const Home = () => (
  <div className="home">
    <section className="hero-container">
      <div className="hero">
        <div>
          <h1 className="text-center">Hi, I'm Sam,<br/>full stack developer</h1>
          <Escher/>
        </div>
      </div>
      <div className="arrow justify">
        <OpenContact>
          <Button>Yes, I'm available for hire.</Button>
        </OpenContact>
      </div>
    </section>
    <section className="container home-icons">
      <h3 className="text-center bold">My Expertise</h3>
      <p>This site is hosted on a 512MB Ubuntu server and powered by (Nginx) React/Redux and Node/Express/Mongo.</p>
      <div className="icon-grid">
        <div>
          <i className="fa fa-magic fa-3x"/>
          <h5>UI/UX</h5>
          <p>React and flux like architecture to drive declarative, complex apps</p>
        </div>
        <div>
          <i className="fa fa-file-o fa-3x"/>
          <h5>Single-Page Application</h5>
          <p>Minified, dynamic web apps that feel like desktop applications.</p>
        </div>
        <div>
          <i className="fa fa-server fa-3x"/>
          <h5>Server Side Rendering</h5>
          <p>Sever generated HTML optimizes SEO and improves load time.</p>
        </div>
      </div>
    </section>
    <section className="container dark arrow">
      <div className="content-div">
        <div>
          <h1>Past Work</h1>
          <p>
            <span>2016 </span>
            This unfortunately short lived fin-tech startup gave me my start.
            Plousio was a loan marketplace designed to help business owners shop
            for the best possible loan product according to their needs and profile.
            I was one of two software engineers tasked with building all frontend and backend systems across
            numerous projects. The screenshot below shows the partner program dashboard - designed
            as part of a financial incentive for business partners to refer clients to the platform.
          </p>
        </div>
      </div>
      <div className="content-image">
        <div>
          <img src="/public/plousio.png"/>
        </div>
        <div className="content-facts">
          <div>
            <div className="justify">
              <h4>PHP-Stripe driven marketplace</h4>
              <p>On-demand app abstraction to facilitate payment between customers (lenders), merchants
                (referral partners) and the platform (Plousio).</p>
            </div>
          </div>
          <div>
            <div className="justify">
              <h4>React/Redux based dashboard</h4>
              <p> Configurable user interface included different feature sets for four different user types:
                clients (business owners), customers, merchants and platform admins.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section className="container text-center light">
      <div className="col-xs-12 col-sm-8 col-sm-offset-2">
        <h3 className="bold">Tools of choice</h3>
        {icons.map(icon => (
          <i key={icon} title={icon} className={"devicon-" + icon.toLowerCase() + "-plain"}/>
        ))}
      </div>
    </section>
  </div>
);

export default withRouter(Home);
