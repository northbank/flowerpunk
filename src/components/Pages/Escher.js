import React from 'react';
import Sphere from './../Organisms/SphereSpirals';

class Escher extends React.Component {
  constructor(props) {
    super(props);

    this.setSize = this.setSize.bind(this);
    this.onClick = this.onClick.bind(this);

    this.state = {
      width: 0,
      spin: true
    }
  }

  setSize() {
    this.setState({
      width: window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth
    });
  }

  onClick() {
    this.setState(prevState => {
      return {
        spin: !prevState.spin
      }
    })
  }

  componentDidMount() {
    this.setSize();

    window.addEventListener('resize', this.setSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setSize);
  }

  render() {
    const iconClass = this.state.spin ? "fa-pause" : "fa-play";

    return (
      <div className="escher transform-center">
        <i
          onClick={this.onClick}
          className={"fa fa-2x pause " + iconClass}
          aria-hidden="true" />
        <Sphere
          spin={this.state.spin}
          radius={Math.min(300, this.state.width / 2 - 40)} />
      </div>
    );
  }
}

export default Escher;