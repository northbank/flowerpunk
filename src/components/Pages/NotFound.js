import React from 'react';
import {NavLink} from 'react-router-dom';

class NotFound extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="container text-center">
        <div className="col-xs-12 col-md-6 col-md-offset-3">
          <div className="jumbotron">
            <h1>404 Page not found</h1>
            <h2>I messed up</h2>
            <h3>{this.props.location.pathname} does not exist</h3>
            <NavLink to="/">
              <h4>Go home</h4>
            </NavLink>
          </div>
        </div>
      </div>
    );
  }
}

export default NotFound;