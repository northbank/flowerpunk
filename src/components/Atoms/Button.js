import React from 'react';

class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div
      id={this.props.id}
      onClick={this.props.onClick}
      className="button">{this.props.children}</div>;
  }
}

Button.defaultProps = {
  onClick: null
};

export default Button;