import React from 'react';
import {openContact} from '../../actions/contact';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'

class OpenContact extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { children, openContact } = this.props;

    return (
      <span onClick={openContact}>
        {children}
      </span>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    openContact: bindActionCreators(openContact, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(OpenContact);