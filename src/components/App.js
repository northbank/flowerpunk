import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Nav from './Organisms/Nav';
import RouteWithSubRoutes from './Molecules/RouteWithSubRoutes';
import Footer from './Organisms/Footer';
import routes from '../configs/routes';
import NotFound from './Pages/NotFound';

const App = () => (
  <div>
    <Nav routes={routes}/>
    <Switch>
      {routes.map((route, i) => (
        <RouteWithSubRoutes {...route} key={i}/>
      ))}
      <Route component={NotFound}/>
    </Switch>
    <Footer />
  </div>
);

export default App;