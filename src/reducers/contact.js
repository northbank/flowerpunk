import {
  UPDATE_PAYLOAD,
  CONTACT_FAILURE,
  CONTACT_INIT,
  CONTACT_SUCCESS,
  OPEN_CONTACT,
  CLOSE_CONTACT
} from '../actions/action-types';

const INITIAL_STATE = {
  email: '',
  name: '',
  message: ''
};

const menu = (state = {payload: INITIAL_STATE}, action) => {
    switch (action.type) {
      case UPDATE_PAYLOAD:
        return Object.assign({}, state, {
          payload: payload(state.payload, action)
        });
      case CONTACT_INIT:
        return Object.assign({}, state, {
          loading: true
        });
      case CONTACT_SUCCESS:
        return Object.assign({}, state, {
          loading: false,
          success: true
        });
      case CONTACT_FAILURE:
        return Object.assign({}, state, {
          loading: false,
          error: action.error
        });
      case OPEN_CONTACT:
        return Object.assign({}, state, {
          open: true
        });
      case CLOSE_CONTACT:
        return Object.assign({}, state, {open: false});
      default:
        return state;
    }
  }
  ;

const payload = (state = INITIAL_STATE, action) => {
  return Object.assign({}, state, {
    [action.id]: action.text
  });
};

export default menu;