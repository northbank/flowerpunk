import { combineReducers } from 'redux'

import featuresByCollection from './featuresByCollection';
import contact from './contact';

const rootReducer = combineReducers({featuresByCollection, contact});

export default rootReducer;