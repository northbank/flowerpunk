import Home from '../components/Pages/Home';
import Examples from '../components/Pages/Examples';
import NationalParks from '../components/Pages/NationalParks';
import Escher from '../components/Pages/Escher';

export default [
    {
        path: '/',
        title: 'Home',
        component: Home,
        exact: true
    }, {
        path: '/examples',
        title: 'Examples',
        component: Examples,
        exact: false,
        routes: [
            {
                path: '/examples/national-parks',
                title: 'National Parks',
                component: NationalParks,
                description: 'Google Maps React component used to display the boundaries of all U.S. national parks',
                img: 'https://upload.wikimedia.org/wikipedia/commons/1/1d/US-NationalParkService-Logo.svg'
            },
            {
                path: '/examples/escher',
                title: 'Sphere Spirals',
                component: Escher,
                description: 'A famous M.C. Escher print recreated using React and d3',
                img: 'http://www.mcescher.com/wp-content/uploads/2013/10/LW428-MC-Escher-Spher-Spirals-1958.jpg'
            }
        ]
    }
];