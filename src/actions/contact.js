import fetch from 'isomorphic-fetch';
import {getApiUrl} from '../utilities/environment';

import {
  CONTACT_INIT,
  CONTACT_FAILURE,
  CONTACT_SUCCESS,
  UPDATE_PAYLOAD,
  OPEN_CONTACT,
  CLOSE_CONTACT
} from './action-types';

export function contact(body) {
  return dispatch => {
    dispatch(contactStart());

    return fetch(getApiUrl() + 'contact', {
      method: "POST",
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        return response.json();
      })
      .then(handleErrors)
      .then(() => {
        dispatch(contactSuccess());
      })
      .catch(err => {
        dispatch(contactFailure(err));
      })
  }
}

function handleErrors(response) {
  if (response.success === false) {
    throw Error(response.error);
  }

  return response;
}

function contactStart() {
  return {
    type: CONTACT_INIT
  }
}

function contactSuccess() {
  return {
    type: CONTACT_SUCCESS
  }
}

function contactFailure(err) {
  return {
    type: CONTACT_FAILURE,
    error: err
  }
}

export function updatePayload(id, text) {
  return {
    type: UPDATE_PAYLOAD,
    id: id,
    text: text
  }
}

export function openContact(page) {
  return {
    type: OPEN_CONTACT,
    page: page
  }
}

export function closeContact() {
  return {
    type: CLOSE_CONTACT
  }
}